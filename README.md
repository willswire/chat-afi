> :warning: **This project has been deprecated**: Download the [AFI Explorer app](https://afiexplorer.com) to use ChatAFI

# ChatAFI

Quickly reference Air Force Instructions, regulations and other publications with the power of [OpenAI's ChatGPT](https://chat.openai.com).

## Overview

This project is a [ChatGPT Retrieval Plugin](https://github.com/openai/chatgpt-retrieval-plugin) for querying the [publicly released Air Force regulations](https://www.e-publishing.af.mil/Product-Index/), running on a [Cloudflare Worker](https://developers.cloudflare.com/workers/).

The documents (text exports of AFIs) are converted into [embeddings](https://platform.openai.com/docs/guides/embeddings) via the [collector](collector/) by way of CI/CD, so that relevant text can be retrieved quickly when a query comes through. These embedding vectors are stored in [KV](https://developers.cloudflare.com/workers/learning/how-kv-works/), Cloudflare's low-latency, key-value data store.

## Getting Started

If you currently have access to ChatGPT Plus, you can access the hosted version of this plugin by installing `chat.afiexplorer.com` as an unverified plugin. More on that process [here](https://platform.openai.com/docs/plugins/introduction).
