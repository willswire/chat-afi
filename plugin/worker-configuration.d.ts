interface Env {
  readonly OPENAI_API_KEY: string; // secret
  readonly KV: KVNamespace;
  readonly R2: R2Bucket;
}
