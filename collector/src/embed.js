import { readFileSync, writeFileSync } from "node:fs";
import { fetchChunkEmbeddings } from "./docs.js";
import { VectorCollection } from "./vector.js";

const { OPENAI_API_KEY } = process.env;

if (OPENAI_API_KEY == null) {
  console.error("OPENAI_API_KEY must be set");
  process.exit(1);
}

/** @type {Chunk[]} */
const chunks = readFileSync("../build/chunks.ndjson", "utf8")
  .trim()
  .split("\n")
  .map((line) => JSON.parse(line));

main().catch(console.error);

async function main() {
  const chunkEmbeddings = await fetchChunkEmbeddings(
    OPENAI_API_KEY ?? "",
    chunks
  );

  const vectorCollection = VectorCollection.from(chunkEmbeddings);

  console.warn("Number of embeddings:", vectorCollection.length);
  console.warn("Embedding length:", vectorCollection.embeddingLength);

  writeFileSync("../build/embeddings.bin", new Uint8Array(vectorCollection.buffer));
}