import path from "node:path";
import { exec } from 'node:child_process';
import { glob } from 'glob';

const execPromise = (command) => {
  return new Promise((resolve, reject) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      if (stderr) {
        reject(new Error(stderr));
        return;
      }
      resolve(stdout);
    });
  });
};

const processFiles = async () => {
  try {
    let files = glob.sync('../build/**/*.pdf');
    let filesCount = files.length;

    let nodeIndex = parseInt(process.env.CI_NODE_INDEX || '0');
    let nodeTotal = parseInt(process.env.CI_NODE_TOTAL || '1');

    let chunkSize = Math.floor(filesCount / nodeTotal);

    let chunkStart = nodeIndex * chunkSize;
    let chunkEnd = Math.min((nodeIndex + 1) * chunkSize, filesCount);

    for (let i = chunkStart; i < chunkEnd; i++) {
      let file = files[i];
      console.log(`Parsing ${i + 1} of ${filesCount} pdfs...`);
      let outputFilename = path.join(path.dirname(file), path.basename(file, path.extname(file))) + '.txt';

      try {
        await execPromise(`java -jar /pdfbox-app-3.0.0.jar export:text -i ${file} -o ${outputFilename}`);
        console.log(`Successfully processed ${file}`);
      } catch (error) {
        console.error(`Error processing ${file}: ${error.message}`);
      }
    }
  } catch (error) {
    console.error(`Error occurred: ${error}`);
  }
};

processFiles();
