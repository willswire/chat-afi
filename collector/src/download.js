import fs from "node:fs";
import path from "node:path";
import axios from "axios";

const dir = '../build';

const makeDirectory = async () => {
  if (!fs.existsSync(dir)) {
    await fs.promises.mkdir(dir, { recursive: true });
  }
};

const downloadDocument = async (url, filename) => {
  const response = await axios({
    url,
    responseType: 'stream',
  });

  return new Promise((resolve, reject) => {
    response.data.pipe(fs.createWriteStream(path.join(dir, filename)))
      .on('finish', resolve)
      .on('error', reject);
  });
};

const downloadDocuments = async () => {
  try {
    const response = await axios.get('https://api.afiexplorer.com/v2/air-force/departmental');
    const data = response.data;
    const jsonLength = data.length;

    const nodeIndex = parseInt(process.env.CI_NODE_INDEX || '0');
    const nodeTotal = parseInt(process.env.CI_NODE_TOTAL || '1');

    const chunkSize = Math.floor(jsonLength / nodeTotal);
    const chunkStart = nodeIndex * chunkSize;
    const chunkEnd = Math.min((nodeIndex + 1) * chunkSize, jsonLength);

    for (let i = chunkStart; i < chunkEnd; i++) {
      if (data[i].hasOwnProperty('DocumentUrl')) {
        const url = data[i]["DocumentUrl"];
        const filename = path.basename(url);

        console.log(`Downloading ${filename}... (${i + 1}/${jsonLength})`);

        await downloadDocument(url, filename);
      } else {
        console.log(`No DocumentUrl key in data at index ${i}`);
      }
    }
  } catch (error) {
    console.error(error);
  }
};

const main = async () => {
  await makeDirectory();
  await downloadDocuments();
};

main();
