import { readdirSync, readFileSync, writeFileSync, existsSync, mkdirSync } from "node:fs";
import { join } from "node:path";
import { textToChunks } from "./docs.js";
import { createHash } from "node:crypto";

const buildDir = "../build";

if (!existsSync(buildDir)){
    mkdirSync(buildDir);
}

const files = readdirSync(buildDir, { withFileTypes: true })
  .filter((dirent) => dirent.isFile())
  .map((dirent) => dirent.name);

const objects = files.map((file) => {
  const filePath = join(buildDir, file);
  const fileContents = readFileSync(filePath, "utf8");
  const hash = createHash("md5").update(fileContents).digest("hex");
  return hash + hash.slice(0, 8) + " " + file;
});

// if we process all of the pubs, we run into performance problems
const keywords = ["haf", "dodi", "daf", "afi", "afpd", "afman", "afh"];
const filesWithContents = objects
  .filter(line => line.endsWith(".txt") && keywords.some(keyword => line.includes(keyword)))
  .map(line => ({
    oid: line.slice(0, 40),
    path: line.slice(41),
    text: readFileSync(join(buildDir, line.slice(41)), "utf8"),
  }));

const chunks = filesWithContents
  .map(({ path, oid, text }) => textToChunks(path, oid, text))
  .flat();

console.warn("Number of files: ", filesWithContents.length);
console.warn("Number of chunks: ", chunks.length);

writeFileSync(
  join(buildDir, "chunks.bulk.json"),
  JSON.stringify(
    chunks.map((chunk) => ({
      key: "chunk:" + chunk.id,
      value: JSON.stringify(chunk),
    }))
  )
);

writeFileSync(
    join(buildDir, "chunks.ndjson"),
    chunks.map((chunk) => JSON.stringify(chunk)).join("\n") + "\n"
);