# Collector

Collector is a GitLab project intended to update textual embeddings data hosted by Cloudflare for Chat AFI.

This project is a work-in-progress:

- [x] Download pubs
- [x] Extract text from pubs
- [x] Run the jobs [in parallel](https://docs.gitlab.com/ee/ci/jobs/job_control.html#parallelize-large-jobs) to decrease runtime
- [x] Create textual embeddings
- [x] Push embeddings to Cloudflare for storage in Workers KV
